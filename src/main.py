#!/usr/bin/env python3

import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_name = sys.argv[1]
server_port = int(sys.argv[2])
unix_socket = sys.argv[3]

server_address = (server_name, server_port)

sock.bind(server_address)
sock.listen(1)

while True:
  connection, client_address = sock.accept()
  try:
    data = connection.recv(1024)
    if data:
      with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
        s.connect(unix_socket)
        s.sendall(data)
        answer = s.recv(1024)
      connection.sendall(answer)
  finally:
    connection.close()
