{ python3
, runCommand
}:

runCommand "project_a" { buildInputs = [ python3 ]; } ''
  mkdir -p $out/bin
  cp ${./src/main.py} $out/bin/project_a
  patchShebangs $out
''
