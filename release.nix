let
  sources = import ./nix/sources.nix {};
  pkgs = import sources.nixpkgs {
    overlays = [
      (import ./overlay.nix)
    ];
  };

in

{
  inherit (pkgs) project_a;

  devShell = pkgs.mkShell {
    nativeBuildInputs = with pkgs; [
      niv
      python3
    ];
  };
}
