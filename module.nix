{ config, pkgs, lib, ... }:
let
  cfg = config.services.project_a;
in
{
  options.services.project_a = {
    enable = lib.mkEnableOption "enable Project A service";
    unixSocketPath = lib.mkOption {
      type = lib.types.path;
      description = "UNIX Socket full path to connect to";
    };
    port = lib.mkOption {
      type = lib.types.port;
      default = 8000;
      description = "TCP Port to listen on";
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.services.project_a = {
      description = "Best TCP server in the world";
      wantedBy = [ "multi-user.target" ];
      serviceConfig.ExecStart = "${pkgs.project_a}/bin/project_a 0.0.0.0 ${builtins.toString cfg.port} ${cfg.unixSocketPath}";
    };
  };
}
